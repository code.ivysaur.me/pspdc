# pspdc

![](https://img.shields.io/badge/written%20in-C%2B%2B%20%28PSPSDK%29-blue)

An NMDC hub client for the Sony PSP (homebrew).

Probably the only one of it's kind in existence. There were a few false starts writing it in Lua against the (many) lua runtime engines, but they all had numerous flaws, writing it in C++ turned out to be easier.

Connects over WiFi, chat using an on-screen keyboard.

Includes a signed EBOOT.PBP - no custom firmware required!

Tags: nmdc, homebrew


## Download

- [⬇️ pspdc-1_0.zip](dist-archive/pspdc-1_0.zip) *(5.40 MiB)*
